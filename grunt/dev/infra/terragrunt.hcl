terraform {
  source = "../../../infra/"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  cidr               = "10.31.0.0/16"
  cluster_name       = "demo0"
  cluster_version    = "1.17"
  region             = "eu-north-1"
  namespace = "demo"
  environment = "dev"
  stage = "0"

  public_subnets = [
    "10.31.0.0/23",
    "10.31.2.0/23",
    "10.31.4.0/23",
  ]

  private_subnets = [
    "10.31.16.0/21",
    "10.31.24.0/21",
    "10.31.32.0/21",
  ]

  worker_groups = [
    {
      name               = "wg-0"
      asg_desired_capacity = 1
      instance_type      = "t3.medium"
      spot_price         = "0.0432"
      kubelet_extra_args = "--node-labels=node_pool=wg-0,node.kubernetes.io/lifecycle=spot"
    }
  ]
  db_name                = "db0"
  db_user                = "user0"
}
