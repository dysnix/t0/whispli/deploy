locals {
  secrets = yamldecode(sops_decrypt_file(find_in_parent_folders("secrets.yaml")))
}

generate "s3_backend" {
  path      = "s3_backend.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
terraform {
  backend "s3" {
    bucket      = "w-tf-state"
    key         = "${path_relative_to_include()}/terraform.tfstate"
    access_key = "${local.secrets.aws.access_key}"
    secret_key = "${local.secrets.aws.secret_key}"
    region = "eu-north-1"
  }
}
EOF
}

inputs = {
  aws_s3_access_id  = "${local.secrets.aws.access_key}"
  aws_s3_secret_key = "${local.secrets.aws.secret_key}"
}
