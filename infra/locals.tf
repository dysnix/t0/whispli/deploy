locals {

  tags = { for k, v in merge(var.tags, module.label.tags) : k => v if k != "Name" }

  subnets = concat(var.private_subnets, var.public_subnets)

  subnets_map = {
    public  = var.public_subnets
    private = var.private_subnets
  }

//  map_roles = concat(
//    var.map_roles,
//    [
//      {
//        rolearn  = aws_iam_role.eksadmin.arn
//        username = "eks-admin"
//        groups   = ["system:masters"]
//      },
//    ]
//  )
}
