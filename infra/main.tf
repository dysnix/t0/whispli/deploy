terraform {
  required_version = ">= 0.12.0"
}

provider "aws" {
  version = ">= 2.28.1"
  region  = var.region
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

data "aws_availability_zones" "available" {
}
data "aws_subnet_ids" "all" {
  vpc_id = module.vpc.vpc_id
}

data "aws_partition" current {}
data "aws_caller_identity" "current" {}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.11"
}

module "label" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=master"
  namespace   = var.namespace
  environment = var.environment
  stage       = var.stage
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 2.6"

  name                 = "${module.label.id}-vpc"
  cidr                 = var.cidr
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = var.private_subnets
  public_subnets       = var.public_subnets
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = local.tags

  public_subnet_tags = {
    "kubernetes.io/cluster/${module.label.id}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${module.label.id}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }
}

resource "aws_security_group" "all_worker_mgmt" {
  name_prefix = "all_worker_management"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
      "172.16.0.0/12",
      "192.168.0.0/16",
    ]
  }
}


module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> 12.2.0"

  cluster_name    = module.label.id
  subnets         = module.vpc.private_subnets
  cluster_version = var.cluster_version

  tags   = local.tags
  vpc_id = module.vpc.vpc_id

  worker_groups = var.worker_groups
  worker_additional_security_group_ids = [aws_security_group.all_worker_mgmt.id]

  map_roles    = var.map_roles
  map_users    = var.map_users
  map_accounts = var.map_accounts

  iam_path             = var.iam_path
  permissions_boundary = var.permissions_boundary
}

provider "random" {
  version = "~> 2.2"
}

resource "random_string" "db_password_0" {
  length  = var.generated_password_length
  special  = false
}


module "db" {
  source = "terraform-aws-modules/rds/aws"
  version = "~> 2.18.0"

  identifier = "${module.label.id}-rds"

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine            = "mysql"
  engine_version    = "5.7.30"
  instance_class    = "db.t3.micro"
  allocated_storage = 5
  storage_encrypted = false

  # kms_key_id        = "arm:aws:kms:<region>:<accound id>:key/<kms key id>"
  name     = var.db_name
  username = var.db_user
  password = random_string.db_password_0.result
  port     = "3306"

  vpc_security_group_ids = [module.vpc.default_security_group_id, module.eks.worker_security_group_id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  multi_az = false

  # disable backups to create DB faster
  backup_retention_period = 0

  # DB subnet group
  subnet_ids = data.aws_subnet_ids.all.ids

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = var.db_name

  # Database Deletion Protection
  deletion_protection = false

  tags   = local.tags
}
