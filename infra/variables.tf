variable "region" {
  default = ""
  type    = string
}

variable "namespace" {
  default = ""
  type    = string
}
variable "environment" {
  default = ""
  type    = string
}
variable "stage" {
  default = ""
  type    = string
}

variable "cidr" {
  default = "VPC Cidr block"
  type    = string
}

variable "cluster_name" {
  description = "Name of the EKS cluster. Also used as a prefix in names of related resources."
  default     = ""
}

variable "cluster_version" {
  description = "EKS Kubernetes version."
}

variable "public_subnets" {
  description = "A list of subnets to place the EKS cluster and workers within."
  type        = list(string)
}

variable "private_subnets" {
  description = "A list of subnets to place the EKS cluster and workers within."
  type        = list(string)
}

variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)

  default = []
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = []
  # [
  #   {
  #     rolearn  = "arn:aws:iam::66666666666:role/role1"
  #     username = "role1"
  #     groups   = ["system:masters"]
  #   },
  # ]
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = []
  # [
  #   {
  #     userarn  = "arn:aws:iam::66666666666:user/user1"
  #     username = "user1"
  #     groups   = ["system:masters"]
  #   },
  #   {
  #     userarn  = "arn:aws:iam::66666666666:user/user2"
  #     username = "user2"
  #     groups   = ["system:masters"]
  #   },
  # ]
}

variable "permissions_boundary" {
  description = "If provided, all IAM roles will be created with this permissions boundary attached."
  type        = string
  default     = null
}

variable "iam_path" {
  description = "If provided, all IAM roles will be created on this path."
  type        = string
  default     = "/"
}

variable "tags" {
  description = "A map of tags to add to all resources."
  type        = map(string)
  default     = {}
}

variable "db_name" {
  description = "Database name"
  type        = string
  default     = "db"
}

variable "db_user" {
  description = "Database root user name"
  type        = string
  default     = "user"
}

variable "generated_password_length" {
  description = "Generated password length"
  default     = 16
  type        = number
}

variable "worker_groups" {
  description = "Map of map of worker groups to create (enchanced)"
  type        = any
  default = [
    {
      name                 = "wg-0"
      instance_type        = "m3.medium"
      asg_desired_capacity = 1
    },
  ]
}
