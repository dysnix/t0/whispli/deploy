ALTER TABLE `tenants`
  ADD `code` VARCHAR(3) NULL DEFAULT null,
  ADD INDEX `tenants_code_unique` (`code`);
