ALTER TABLE `environments`
    ADD `origin_domain` VARCHAR(255) NOT NULL DEFAULT 'd3nfmsoah11r0v.cloudfront.net' COMMENT 'The domain where then application entry point is hosted, such as a CloudFront domain name';
