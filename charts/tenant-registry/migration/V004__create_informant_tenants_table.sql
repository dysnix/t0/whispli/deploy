CREATE TABLE `informant_tenants` (
  `id` char(36) NOT NULL,
  `tenant_id` char(36) NOT NULL,
  `tenant_informant_id` varchar(36) NULL,
  `informant_id` varchar(36) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tenant_id_informant_id_unique` (`tenant_id`, `informant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
