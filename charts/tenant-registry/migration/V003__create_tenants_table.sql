CREATE TABLE `tenants` (
  `id` char(36) NOT NULL,
  `environment_id` char(36) NOT NULL,
  `name` varchar(128) NOT NULL,
  `fqdn` varchar(128) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tenants_fqdn_unique` (`fqdn`),
  KEY `tenants_environment_id_index` (`environment_id`),
  CONSTRAINT `tenants_environment_id_foreign` FOREIGN KEY (`environment_id`) REFERENCES `environments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
