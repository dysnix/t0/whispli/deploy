CREATE TABLE `environments` (
  `id` char(36) NOT NULL,
  `region_id` char(36) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `environments_name_unique` (`name`),
  KEY `environments_region_id_index` (`region_id`),
  CONSTRAINT `environments_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
