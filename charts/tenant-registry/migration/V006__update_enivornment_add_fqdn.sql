ALTER TABLE `environments`
  ADD `fqdn` VARCHAR(128) NOT NULL DEFAULT '',
  ADD INDEX `environments_fqdn_unique` (`fqdn`);
