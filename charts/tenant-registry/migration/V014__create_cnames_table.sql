CREATE TABLE `tenant_cnames` (
  `id` char(36) NOT NULL,
  `tenant_id` char(36) NOT NULL,
  `cname` varchar(128) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tenant_cnames_cname_unique` (`cname`),
  CONSTRAINT `tenant_cnames_tenant_id` FOREIGN KEY (`tenant_id`) REFERENCES tenants (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
