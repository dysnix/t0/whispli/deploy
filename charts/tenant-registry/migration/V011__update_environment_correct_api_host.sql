ALTER TABLE environments ALTER COLUMN api_host SET DEFAULT '/api';
UPDATE environments SET api_host = '/api';
