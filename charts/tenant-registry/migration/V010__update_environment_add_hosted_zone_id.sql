ALTER TABLE `environments`
    ADD `hosted_zone_id` VARCHAR(255) NULL DEFAULT 'Z3LSII9O88YN02' COMMENT 'Used to interface with Route 53 and update records. Defaults to whispli.com record set.';
